defmodule WS_h
do
  @behaviour :cowboy_websocket

  def init(req, state) do
    {:cowboy_websocket, req, state}
  end

  def websocket_handle( _frame, _req, state) do
    {:ok, state}
  end

  def websocket_info(_info, state) do
    {:reply, state}
  end

  def websocket_info(message, req, state) do
    {:reply, {:text, message}, req, state}
  end

  def websocket_handle( {:ping, message}, state) do
    {:reply, {:pong, message}, state}
  end

  def terminate(_reason, _req, _state) do
    :ok
  end

end
