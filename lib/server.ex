defmodule Server
do

  def build_dispatch_config do
    :cowboy_router.compile([ { :_, [ {"/ws", WS_h, [] } ] } ])
  end

  def start(_type, _args)
  do
    dispatch_config = build_dispatch_config()
    {:ok, _} = :cowboy.start_clear(:http, [{:port, 4000}], %{ env: %{dispatch: dispatch_config}} )
  end

end
